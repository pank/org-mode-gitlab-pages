#+title: Publishing org-mode projects on Gitlab Pages
#+author: Rasmus
#+date: <2018-01-22 Mon>

An example of a website written in [[https://orgmode.org][Org]] and exported using the Org
export framework via [[https://about.gitlab.com/features/gitlab-ci-cd/][GitLab CI]] and [[https://about.gitlab.com/features/pages/][GitLab Pages]].

See the published site [[https://pank.gitlab.io/org-mode-gitlab-pages/][here]].

* Org
Org is [[https://www.gnu.org/philosophy/free-sw.en.html][Free Software]] and part of [[https://www.gnu.org/s/emacs/][GNU Emacs]].  It's a simple document
format with a powerful and intuitive interface that can [[https://orgmode.org/org.html#Exporting][export]] and
[[https://orgmode.org/org.html#Publishing][publish]] to HTML, LaTeX, ascii, ODT amongst others.  Using Org Babel,
it can also [[https://orgmode.org/org.html#Working-with-source-code][evaluate source code]].  See the [[https://orgmode.org/org.html][Org manual]] for details.

* Gitlab CI

This project's static Pages are built by [[https://about.gitlab.com/gitlab-ci/][GitLab CI]], following the
steps defined in [[https://gitlab.com/pages/nikola/blob/master/.gitlab-ci.yml][.gitlab-ci.yml]]:

#+BEGIN_SRC conf
  image: pank/docker-ox

  pages:
    script:
    - emacs --batch --no-init-file --load publish.el --funcall org-publish-all
    artifacts:
      paths:
      - public
#+END_SRC

These build instructions are using a docker image, namely
~pank/docker-ox~.  It makes it possible to publish the project on a
public runner on Gitlab.com.

You could probably get a better experience by using a local runner on
a server you control.

* Building locally
Org is part of GNU Emacs.  See how to obtain GNU Emacs on your OS
[[https://www.gnu.org/software/emacs/download.html][here]].

To get the latest version of Org [[https://orgmode.org/][here]].

* GitLab User or Group Pages

To use this project as your user/group website, you will need one
additional step: just rename your project to ~namespace.gitlab.io~,
where ~namespace~ is your ~username~ or ~groupname~. This can be done by
navigating to your project's Settings.

Read more about [[https://docs.gitlab.com/ce/user/project/pages/#user-or-group-pages][user/group Pages]] and [[https://docs.gitlab.com/ce/user/project/pages/#project-pages][project Pages]].

* Did you fork this project?

If you forked this project for your own use, please go to your
project's Settings and remove the forking relationship, which won't be
necessary unless you want to contribute back to the upstream project.
